//
// Created by Sam on 07/11/2019.
//

#ifndef UNTITLED_NODE_H
#define UNTITLED_NODE_H

#include <iostream>

template <typename T>
class Node {
private:
    Node<T> * fg;
    Node<T> * fd;
    T element;

public:


    void insertNode(T element) {
        if (this->element == element) {
            return;
        } else if (this->element > element) {
            if (fg == nullptr) {
                fg = new Node(element);
            } else {
                fg->insertNode(element);
            }
        } else {
            if (fd == nullptr) {
                fd = new Node(element);
            } else {
                fd->insertNode(element);
            }
        }
    }

    bool searchNode(T element) {
        if (this->element == element) {
            return true;
        } else if (this->element > element) {
            if (fg == nullptr){
                return false;
            } else {
                fg->searchNode(element);
            }
        } else {
            if (fd == nullptr){
                return false;
            } else {
                fd->searchNode(element);
            }
        }
    }

    void printNode() {

        if (fg != nullptr) {
            fg->printNode();
        }

        std::cout << " " << element << " ";

        if (fd != nullptr) {
            fd->printNode();
        }
    }


    Node(){
        fg = nullptr;
        fd = nullptr;
        element = T();
    }

    Node(T element) {
        this->element = element;
        fd = nullptr;
        fg = nullptr;
    }

    Node(Node<T> const &node) {
        this->element = node.element;
        if (node.fd == nullptr) {
            this->fd = nullptr;
        } else {
            fd = new Node(*(node.fd));
        }
        if (node.fg == nullptr) {
            this->fg = nullptr;
        } else {
            fg = new Node(*(node.fg));
        }
    }


    bool operator==(const Node &rhs) const {
        return fg == rhs.fg &&
               fd == rhs.fd &&
               element == rhs.element;
    }

    bool operator<(const Node &rhs) const {
        if (fg < rhs.fg)
            return true;
        if (rhs.fg < fg)
            return false;
        if (fd < rhs.fd)
            return true;
        if (rhs.fd < fd)
            return false;
        return element < rhs.element;
    }

    bool operator>(const Node &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Node &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Node &rhs) const {
        return !(*this < rhs);
    }

    bool operator!=(const Node &rhs) const {
        return !(rhs == *this);
    }

    Node<T>& operator= (const Node<T>& node) {
        //On évite l'auto affectation
        if (this == &node) return *this;

        //On cleanse les variables
        delete this->fd;
        delete this->fg;

        //On copie
        this->element = node.element;
        if (node.fd == nullptr) {
            this->fd = nullptr;
        } else {
            fd = new Node(*(node.fd));
        }
        if (node.fg == nullptr) {
            this->fg = nullptr;
        } else {
            fg = new Node(*(node.fg));
        }
        return *this;
    }

    Node<T>* getFg() const {
        return fg;
    }

    ~Node() {
        delete fg;
        delete fd;
    }

    void setFg(Node<T> *fg) {
        this->fg = fg;
    }

    Node<T>* getFd() const {
        return fd;
    }

    void setFd(Node<T> *fd) {
        this->fd = fd;
    }

    T getElement() const {
        return element;
    }

    void setElement(T element) {

        this->element = element;
    }




};




#endif //UNTITLED_NODE_H
