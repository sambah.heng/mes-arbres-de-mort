//
// Created by Sam on 10/11/2019.
//

#ifndef MES_ARBRES_DE_MORT_ITERATOR_HPP
#define MES_ARBRES_DE_MORT_ITERATOR_HPP

#include <stack>
#include "ABR.h"


template <typename T>
class Iterator {

private:
    friend class Node<T>;
    friend class ABR<T>;
    ABR<T>* actual;
    std::stack<Node<T>*> parents;

    Iterator(const ABR<T> &abr, int init) {
       //on va tout à gauche en sauvegardant au passage les adresse
       actual = abr;

       while (actual->root.fg != nullptr) {
           parents.push(actual);
           actual = actual->root.fg;
       }
    }

public:


    Iterator() { actual = nullptr; }

    Iterator(T* it) { actual = it; }

    T& operator*() const { return *actual; }

    T* operator->() const { return &actual; }

    bool operator==(const Iterator &iterator) {
        return *actual == iterator.actual;
    }

    bool operator!=(const Iterator &iterator) {
        return  *actual != iterator.actual;
    }

    Iterator operator++(int) {
        return actual++;
    }

    Iterator& operator++() {
        ++actual;
        return *this;
    }




};


#endif //MES_ARBRES_DE_MORT_ITERATOR_HPP
