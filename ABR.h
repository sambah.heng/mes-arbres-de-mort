//
// Created by Sam on 07/11/2019.
//

#ifndef UNTITLED_ABR_H
#define UNTITLED_ABR_H

#include <stack>
#include "Node.h"
template <typename T>
class Iterator_ABR;

template <typename T>
class ABR {

private:
    Node<T> * root;

public:

    typedef Iterator_ABR<T> iterator;
    friend class Iterator_ABR<T>;

    void insert(T element) {
        if (root == nullptr) {
            root = new Node<T>(element);
        } else {
            root->insertNode(element);
        }
    }

    iterator insertIt(T element) {
        if (root == nullptr) {
            root = new Node<T>(element);
            iterator it = this->begin();
            return it;
        } else {
            root->insertNode(element);
        }
        iterator it = this->begin();
        iterator ite = this->end();
        while (element != *it && it != ite) {
            it++;
        }
        return it;

    }

    bool search(T element) {
        if (root == nullptr) {
            return false;
        } else {
            root->searchNode(element);
        }
    }

    iterator searchIt(T element) {
        if (root == nullptr) {
            return this->end();
        } else {
            if (root->searchNode(element))  {
                iterator it = this->begin();
                iterator ite = this->end();
                while (element != *it && it != ite) {
                    it++;
                }
                return it;
            } else {
                return this->end();
            }
        }
    }

    void printABR() {
        if (root == nullptr) {
            return;
        } else {
            root->printNode();
        }
    }

    ABR() { root = nullptr; }

    ABR(Node<T> *root) : root(root) {}

    ABR(const ABR& arbre) {
       if (arbre.root == nullptr) {
           root = nullptr;
       } else {
           root = new Node<T>(*arbre.root);
       }
    }

    ABR<T>& operator= (const ABR& arbre){
        if (this == &arbre) return *this;
        delete this->root;
        if (arbre.root == nullptr) {
            root = nullptr;
        } else {
            root = new Node<T>(*arbre.root);
        }
        return *this;
    }

    Node<T>* getRoot() {
        return this->root;
    }

    ~ABR() {
        delete root;
    }


    iterator begin() {
        return iterator(*this);
    }

    iterator end() {
        return iterator();
    }

};


template <typename T>
class Iterator_ABR {

private:
    friend class Node<T>;

    friend class ABR<T>;

    std::stack<Node<T>*> parents;

    //Constructeur qui nous met au début
    Iterator_ABR(const ABR<T> &abr) {
        Node<T>* actual = abr.root;
        while (actual != nullptr) {
            parents.push(actual);
            actual = actual->getFg();
        }
    }

public:

    Iterator_ABR() {
        parents = std::stack<Node<T>*>();
    }

    T* operator->() const { return &(parents.top()->getElement()); }
    T operator*() const { return parents.top()->getElement(); }

    bool operator==(const Iterator_ABR &iterator) {
        return parents == iterator.parents;
    }


    bool operator!=(const Iterator_ABR &iterator) {
        return  parents != iterator.parents;
    }

    Iterator_ABR operator++(int) {
        Node<T>* actual = parents.top()->getFd();
        parents.pop();
        while (actual != nullptr) {
            parents.push(actual);
            actual = actual->getFg();
        }
        return *this;
    }

    Iterator_ABR& operator++() {
        Node<T>* actual = parents.top()->getFd();
        parents.pop();
        while (actual != nullptr) {
            parents.push(actual);
            actual = actual->getFg();
        }
        return *this;
    }

};


#endif //UNTITLED_ABR_H
